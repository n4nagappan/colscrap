# Scrapy settings for colscrap project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'colscrap'

SPIDER_MODULES = ['colscrap.spiders']
NEWSPIDER_MODULE = 'colscrap.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'colscrap (+http://www.yourdomain.com)'
