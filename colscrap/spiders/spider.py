from scrapy.spider import Spider
from scrapy.http import Request
from scrapy.http import FormRequest
from scrapy.selector import Selector
from colscrap.items import CollegeInfo

from scrapy.item import Field

# This will not work well with item loaders
# TODO: Find an alternative later to create dynamic item fields
class FlexibleCollegeInfo(CollegeInfo):
    def __setitem__(self,key,value):
         if key not in self.fields:
             self.fields[key] = Field()
         self._values[key] = value

homePage = "http://www.schoolcolleges.com/"
statesLink = "http://www.schoolcolleges.com/state.php?data=country1&val=-1"
getCitiesUrl = "http://www.schoolcolleges.com/state1.php?data1=states&val1="
baseUrl = "http://www.schoolcolleges.com/college.select.php"

class Utils:
    @staticmethod
    def convertArrayToObject(array):
        obj = {}
        currentKey = ""
        for i,a in enumerate(array):
            if ":" in a:
                pos = a.index(":")
                currentKey = a[:pos].lower()
                obj[currentKey] = ""
            else:
                obj[currentKey] += a + " "

        return obj

class ColSpider(Spider):
    name = "col"

    def start_requests(self):
        #Fetch first page
        yield Request(statesLink,callback=self.parse_states)
        #yield FormRequest("http://www.schoolcolleges.com/college.select.php",
        #   formdata={'select1':'TAMILNADU','select2[]':'0'},
        #   callback=self.process_pag#e)
                                     #
    def parse_states(self,response): #
        #print response.body         #
        html = "<html>" + response.body + "</html>"
        #print "******\n",html
        sel = Selector(text=html)
        states = sel.xpath("//option/@value").extract()
        #remove "--Choose State--" option
        states.pop(0) 
        
        print states
        for state in states:
            request = Request(getCitiesUrl + state,callback=self.parse_cities)
            request.meta['state'] = state
            yield request

    def parse_cities(self,response):
        state = response.meta['state']
        #print "$$$$$$$$$----------------", state
        html = "<html>" + response.body + "</html>"
        sel = Selector(text=html)
        cities = sel.xpath("//option/@value").extract()
        #remove "0" value for all cities
        cities.pop(0)

        print cities
        for city in cities:
            formRequest = FormRequest("http://www.schoolcolleges.com/college.select.php",
                formdata={'select1':state,'select2[]':city},
                callback=self.process_page)
            formRequest.meta['state'] = state
            formRequest.meta['city']  = city
            yield formRequest

    def process_page(self,response):
        state = response.meta['state']
        city  = response.meta['city']
        #1. Process the current page
        print "page retrieved : "+response.url
        filename = "result"
        sel = Selector(response)
        
        colleges = sel.xpath('//span[@class="text1"]')

        for college in colleges:
            collegeInfo = FlexibleCollegeInfo()
            collegeInfo['name'] = college.xpath('text()').extract()[0].strip()
            collegeInfo['state'] = state.lower().strip()
            collegeInfo['city'] = city.lower().strip()
            moreData = college.xpath('../following-sibling::td//text()').extract()
            obj =  Utils.convertArrayToObject(moreData)
            for key in obj:
                collegeInfo[key] = obj[key].strip()
                
            print "+++++",collegeInfo
            yield collegeInfo

        #2. find next url to be processed
        links = sel.xpath('//a[@href and text()="Next"]/@href').extract()
        if len(links) == 0:
            return

        nextPageUrl = baseUrl + links[0]
        print "\n***** next url to be processed *********: " + nextPageUrl 
        request =  Request(nextPageUrl , callback=self.process_page)
        # set data to be passed on to the callback
        request.meta['state'] = state
        request.meta['city']  = city
        yield request

    def parse(self,response):
        filename = "result"
        open(filename,'wb').write(response.body)
        pass
    
